CURRENT_DIRECTORY := $(shell pwd)

define venv_command
	.venv/bin/python $(1)
endef

define coverage_command
	.venv/bin/coverage $(1)
endef

.PHONY: build-env start test

build-env:
	rm -rf .venv
	virtualenv -p /usr/bin/python3 .venv
	.venv/bin/pip install -r requirements.txt

start:
	$(call venv_command,src/main.py)

test:
	rm -rf .coverage htmlcov
	$(call coverage_command,run -m unittest discover src/)
	$(call coverage_command,html)
	rm -rf .coverage
	xdg-open htmlcov/index.html
