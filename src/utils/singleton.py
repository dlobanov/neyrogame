class Singleton(object):
    __class = None
    __instance = None

    def __init__(self, cls):
        self.__class = cls

    def __call__(self, *args, **kwargs):
        if not self.__instance:
            self.__instance = self.__class(*args, **kwargs)
        return self.__instance
