class ActorState:

    def __init__(self):
        self._speed = 0
        self._weight = 0
        self._health = 0
        self._coordinates = 0
        self._direction = 0

    @property
    def coordinates(self):
        return self._coordinates

    @coordinates.setter
    def coordinates(self, value):
        self._coordinates = value

    @property
    def weight(self):
        return self._weight

    @weight.setter
    def weight(self, value):
        self._weight = value

    @property
    def health(self):
        return self._health

    @health.setter
    def health(self, value):
        self._health = value

    @property
    def direction(self):
        return self._direction

    @direction.setter
    def direction(self, value):
        self._direction = value

    @property
    def speed(self):
        return self._speed

    @speed.setter
    def speed(self, value):
        self._speed = value




    @property
    def state(self):
        return {
            "speed": self.speed,
            "weight": self.weight,
            "health": self.health,
            "coordinates": self.coordinates,
            "direction": self.direction
        }
