import math


class ScalarException(Exception):
    pass


class PointException(ScalarException):
    pass


class PointAddException(PointException):
    pass


class Point:
    def __init__(self, x, y):
        self._x = x
        self._y = y

    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    @property
    def coordinates(self):
        return self._x, self._y

    def __add__(self, other):
        if isinstance(other, Vector):
            return Path(
                start_position=self,
                length=other.length,
                direction=other.direction
            )

        if isinstance(other, Point):
            return Path(
                start_position=self,
                final_position=other
            )

        raise PointAddException("Unable to add '{}' to Point.".format(other.__class__.__name__))

    def __str__(self):
        return "<Point ({x}; {y})>".format(
            x=self.x,
            y=self.y
        )

    def __repr__(self):
        return str(self)


class PathException(ScalarException):
    pass


class PathInitializationException(PathException):
    pass


class PathAddException(PathException):
    pass


class Path:
    def __init__(self, start_position: Point,
                 final_position: Point = None,
                 length: float = None, direction: float = None):

        self._start = start_position

        if isinstance(final_position, Point):
            self._end = final_position
            self._length = None
            self._direction = None
        elif None not in (length, direction,):
            self._length = length
            self._direction = direction
            self._end = None
        else:
            raise PathInitializationException("Unable to create Path")

    def __add__(self, other):
        if isinstance(other, Point):
            return Path(
                start_position=self.start_point,
                final_position=other
            )

        if isinstance(other, Vector):
            second_path = self.final_point + other
            return Path(
                start_position=self.start_point,
                final_position=second_path.final_point
            )

        raise PathAddException("Unable to add '{}' to Path.".format(other.__class__.__name__))

    def __str__(self):
        return "<Path ({x0}; {y0})->({x1}; {y1}), length {len}, direction {direction}>".format(
            x0=self.start_point.x,
            y0=self.start_point.y,
            x1=self.final_point.x,
            y1=self.final_point.y,
            len=self.length,
            direction=self.direction
        )

    def __repr__(self):
        return str(self)

    def _coordinates_by_alpha_c(self, alpha, c):

        """
        Calculates catheters of the right triangle by hypotenuse and x catheter angle
        :param alpha: catheter angle
        :param c: hypotenuse length
        :return: x and y catheters length
        """

        a = math.sin(alpha) * c  # y
        b = math.sin(math.pi / 2 - alpha) * c  # x
        return b, a  # x, y

    def _add_path(self, length, direction):
        """
        Sets actor's position to new position by adding path
        :param length: path length
        :param direction: path direction
        :return: None
        """
        x, y = 0, 0

        # Reduces direction to be lower than 2*PI
        direction = direction % (math.pi * 2)

        # Defining direction
        if direction == 0:
            x = length
        elif direction < math.pi / 2:
            # < 90
            x, y = self._coordinates_by_alpha_c(alpha=direction, c=length)
        elif direction == math.pi / 2:
            # == 90
            y = length
        elif direction < math.pi:
            # < 180
            x, y = self._coordinates_by_alpha_c(alpha=(math.pi - direction), c=length)
            x *= -1
        elif direction == math.pi:
            # == 180
            x = -length
        elif direction < math.pi * 1.5:
            # < 270
            x, y = self._coordinates_by_alpha_c(alpha=(direction - math.pi), c=length)
            x, y = -x, -y
        elif direction == math.pi * 1.5:
            # == 270
            y = -length
        else:
            # < 360
            x, y = self._coordinates_by_alpha_c(alpha=(2 * math.pi - direction), c=length)
            y = -y

        return self._start.x + x, self._start.y + y

    def _angle_by_2_coordinates(self, x0, y0, x1, y1, distance):
        dx = x1 - x0
        dy = y1 - y0

        if dx < 0:
            if dy < 0:
                return math.pi + self.__x_catheter_angle_by_c_dy(c=distance, dy=abs(dy))
            if dy == 0:
                return math.pi
            else:
                return math.pi - self.__x_catheter_angle_by_c_dy(c=distance, dy=abs(dy))
        elif dx == 0:
            if dy < 0:
                return math.pi * 1.5
            if dy == 0:
                return 0
            else:
                return math.pi / 2
        else:
            if dy < 0:
                return 2 * math.pi - self.__x_catheter_angle_by_c_dy(c=distance, dy=abs(dy))
            elif dy == 0:
                return 0
            else:
                return self.__x_catheter_angle_by_c_dy(c=distance, dy=abs(dy))

    def __x_catheter_angle_by_c_dy(self, c, dy):
        return math.asin(dy / c)

    @property
    def start_point(self):
        return self._start

    @property
    def final_point(self):
        if self._end is None:
            self._end = Point(*self._add_path(length=self._length, direction=self._direction))
        return self._end

    @property
    def length(self):
        if self._length is None:
            self._length = math.sqrt(
                (self.start_point.x - self.final_point.x) ** 2 +
                (self.start_point.y - self.final_point.y) ** 2
            )
        return self._length

    @property
    def direction(self):
        if self._direction is None:
            self._direction = self._angle_by_2_coordinates(
                x0=self.start_point.x,
                y0=self.start_point.y,
                x1=self.final_point.x,
                y1=self.final_point.y,
                distance=self.length
            )
        return self._direction


class VectorException(ScalarException):
    pass


class VectorAddException(VectorException):
    pass


class Vector:
    def __init__(self, length, direction):
        self._length = length
        self._direction = direction

    def __add__(self, other):
        if isinstance(other, Vector):
            path = Point(0, 0) + self
            path = path + other
            return Vector(
                length=path.length,
                direction=path.direction
            )

        if isinstance(other, (Point, Path,)):
            return other + self

        raise VectorAddException("Unable to add '{}' to Vector.".format(other.__class__.__name__))

    @property
    def direction(self):
        return self._direction

    @property
    def length(self):
        return self._length
