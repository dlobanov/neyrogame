import math


class Equation:

    def __init__(self, a=0, b=0, c=0):
        self._a = a
        self._b = b
        self._c = c
        self._x1 = None
        self._x2 = None

    def _calculate(self):
        if None in (self._x1, self._x2):
            d = self._b ** 2 - 4 * self._a * self._c

            if d > 0:
                d = math.sqrt(d)

                self._x1 = (-self._b + d) / (2 * self._a)
                self._x2 = (-self._b - d) / (2 * self._a)
            else:
                self._x1 = self._x2 = 0

    @property
    def get_min_positive(self):
        self._calculate()

        items = [i for i in (self._x1, self._x2,) if i > 0]

        if items:
            return min(items)

        return 0

    @property
    def results(self):
        self._calculate()
        return self._x1, self._x2
