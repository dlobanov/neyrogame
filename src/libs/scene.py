import math
import uuid
import pyglet

import pyglet.window.mouse as mouse

from random import randint

from utils.singleton import Singleton
from libs.equation import Equation
from libs.image_provider import ImageProvider
from libs.scalar import Point, Path, Vector


@Singleton
class Scene(pyglet.window.Window):
    DENSITY = 1.002
    VISCOSITY = 0.00010002
    SCALE = 10 * 1000

    def __init__(self, name, width, height, debug=False):
        super().__init__(width=width, height=height, caption=name)
        self._selected_scene_actor = None
        self._label = None
        self._scene_actors = {}
        self._debug = debug

    def add_scene_actor(self, scene_actor):
        sa_uuid = uuid.uuid4()
        while sa_uuid in self._scene_actors:
            sa_uuid = uuid.uuid4()
        scene_actor.uuid = sa_uuid
        self._scene_actors[sa_uuid] = scene_actor

    def on_draw(self):
        self.clear()
        for actor in self._scene_actors.values():
            actor.draw()

        if self._selected_scene_actor:
            current_scene_actor = self._scene_actors[self._selected_scene_actor]
            label = pyglet.text.HTMLLabel(
                """
                <font color=\"#FFFFFF\">
                   UUID: {uuid}<br>
                   speed: {speed}<br>
                   direction: {direction}
                </font>
                """.format(
                    uuid=str(self._selected_scene_actor),
                    speed=current_scene_actor.speed,
                    direction=current_scene_actor.direction
                ),
                x=5,
                y=self.height-15,
                width=self.width,
                multiline=True
            )
            label.draw()

    def update(self, dt):
        for actor in self._scene_actors.values():
            actor.update(dt)

    def run(self):
        pyglet.clock.schedule_interval(self.update, 1 / 120.0)
        pyglet.app.run()

    def _normalize_coordinates(self, a, b, zero_point):
        if abs(a - b) > -(-zero_point // 2):
            shift_d = zero_point - max(a, b)
            a = (a + shift_d) % zero_point
            b = (b + shift_d) % zero_point
        return a, b

    def get_distance_and_angle(self, x0, y0, x1, y1):
        x0, x1 = self._normalize_coordinates(x0, x1, self.width)
        y0, y1 = self._normalize_coordinates(y0, y1, self.height)

        path = Path(Point(x0, y0), Point(x1, y1))

        return path.length, path.direction

    def find(self, x, y, visibility_radius, radius=0):
        result = {}
        visibility_radius += radius
        for actor in self._scene_actors.values():
            distance, angle = self.get_distance_and_angle(x0=x, y0=y, x1=actor.x, y1=actor.y)
            if distance <= visibility_radius:
                result[actor.uuid] = distance
        return result

    def on_mouse_press(self, x, y, button, modifiers):
        if button == mouse.LEFT:
            self.mouse_selection(x=x, y=y)

    def mouse_selection(self, x, y):
        b = self.find(x, y, 5)
        if len(b) == 1:
            self._selected_scene_actor = list(b.keys())[0]
        else:
            self._selected_scene_actor = None

    def get_pixel_size(self, meter):
        return meter * self.SCALE

    @property
    def debug(self):
        return self._debug


class SceneActor(pyglet.sprite.Sprite):
    RESISTANCE_COEFFICIENT_BALL = 1

    def __init__(self, weight_kg=1, radius=0.05, color=None):
        self._scene = Scene()
        self._image_provider = ImageProvider()

        self._uuid = None
        self._speed = 0
        self._direction = 0
        self._force = 0
        self._force_direction = 0

        self._radius = radius
        self._weight = weight_kg

        self._color = color

        img = self._image_provider.get_image(color=color)
        super(SceneActor, self).__init__(
            img=pyglet.image.load(img)
        )

        self._update_size()
        self.position = (randint(0, self._scene.width), randint(0, self._scene.height))

    def update(self, dt):
        if self._scene.debug:
            print("Position:", self.position)
            print("Speed", self._speed)
            print("Direction", self._direction)

        radius_before = self.radius
        initial_point = Point(*self.position)

        # Add Speed path vector
        position = Path(
            start_position=initial_point,
            length=(self._speed * dt),
            direction=self._direction
        )

        # Calculate and add Force path vector
        force_acceleration = self._force / self.mass
        force_path = force_acceleration * (dt ** 2) / 2

        position += Vector(
            length=force_path,
            direction=self._force_direction
        )

        # Calculate and add Environment Force path vector
        sa_movement, sa_direction = self._scene.get_distance_and_angle(
            x0=initial_point.x,
            y0=initial_point.y,
            x1=position.final_point.x,
            y1=position.final_point.y
        )
        sa_speed = sa_movement / dt

        # Calculating environment resistance force
        erf = self._environment_resistance_force(speed=sa_speed)
        erf_acceleration = erf / self.mass
        erf_path = erf_acceleration * (dt ** 2) / 2

        if erf_path >= sa_movement:
            # Kinetic energy
            # E = (m * u ** 2) / 2
            e_kin = self.mass * sa_speed ** 2 / 2
            # F from E
            # F ** 2 = (2 * E * m) / t ** 2
            f_body = math.sqrt((2 * e_kin * self.mass) / dt ** 2)

            equation = Equation(
                a=self._scene.DENSITY * self.square / 2,
                b=6 * math.pi * self._scene.VISCOSITY * self.radius,
                c=-f_body
            )

            erf_speed = max(*[abs(i) for i in equation.results])

            # self._speed = equation.get_min_positive
            position = Path(
                start_position=initial_point,
                length=erf_speed * dt,
                direction=sa_direction
            )
        else:
            position += Vector(
                length=erf_path,
                direction=sa_direction + math.pi
            )

        # Calculate SA direction and speed
        movement, self._direction = self._scene.get_distance_and_angle(
            x0=initial_point.x,
            y0=initial_point.y,
            x1=position.final_point.x,
            y1=position.final_point.y
        )
        self._speed = movement / dt
        self.position = position.final_point.coordinates

        if radius_before != self.radius:
            self._update_size()

        self._check_bounds()

    def _check_bounds(self):
        min_val = 0
        max_x = self._scene.width
        max_y = self._scene.height
        if self.x < min_val:
            self.x = max_x
        elif self.x > max_x:
            self.x = min_val
        if self.y < min_val:
            self.y = max_y
        elif self.y > max_y:
            self.y = min_val

    def _environment_resistance_force(self, speed):
        # Stocks
        # F = 6 * PI * n * r * v
        resistance_force = 6 * math.pi * self.radius * self._scene.VISCOSITY * speed
        # Frontal resistance
        # F = PI * S * v ** 2 / 2
        frontal_resistance = self.RESISTANCE_COEFFICIENT_BALL * self._scene.DENSITY * (speed ** 2) * self.square / 2
        return resistance_force + frontal_resistance

    def _update_size(self):
        self.scale = self.diameter / self._image_provider.DIAMETER

    @property
    def position(self):
        shift = self.diameter // 2
        return self._x + shift, self._y + shift

    @position.setter
    def position(self, pos):
        shift = self.diameter // 2
        self._x, self._y = pos[0] - shift, pos[1] - shift
        self._update_position()

    @property
    def diameter(self):
        return int(self._scene.get_pixel_size(meter=self.radius) * 2) or 1

    @property
    def uuid(self):
        if uuid is None:
            raise ValueError("UUID should not be None.")
        return self._uuid

    @uuid.setter
    def uuid(self, value):
        if self._uuid:
            raise ValueError("UUID was clarified earlier.")
        self._uuid = value

    @property
    def x(self):
        return self._x + self.diameter // 2

    @x.setter
    def x(self, value):
        self._x = value - self.diameter // 2
        self._update_position()

    @property
    def y(self):
        return self._y + self.diameter // 2

    @y.setter
    def y(self, value):
        self._y = value - self.diameter // 2
        self._update_position()

    @property
    def speed(self):
        return self._speed

    @property
    def direction(self):
        return self._direction

    @property
    def radius(self):
        return self._radius

    @property
    def square(self):
        return math.pi * (self.radius ** 2)

    @radius.setter
    def radius(self, value):
        self._radius = value

    @property
    def force(self):
        return self._force

    @force.setter
    def force(self, value):
        self._force = value

    @property
    def force_angle(self):
        return self._force_direction

    @force_angle.setter
    def force_angle(self, value):
        self._force_direction = value

    @property
    def mass(self):
        return self._weight

    @mass.setter
    def mass(self, value):
        self._weight = value


class Amoeba(SceneActor):
    DENSITY = 1070

    @property
    def bulk(self):
        return self.mass / self.DENSITY

    @property
    def radius(self):
        return math.pow((3 * self.bulk) / (4 * math.pi), 1 / 3)

    @radius.setter
    def radius(self, value):
        raise ValueError("Radius will be calculated according to mass and density.")
