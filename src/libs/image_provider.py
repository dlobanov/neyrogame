import os

from random import randint

from PIL import Image, ImageDraw

from utils.singleton import Singleton


@Singleton
class ImageProvider:
    DIAMETER = 100

    def __init__(self, image_folder):
        self._folder = image_folder
        self._used_images = set()
        self._reserved_colors = set()
        self._available_images = self.get_created_images()

    def get_image(self, color=None):
        if not color:
            available = set(self._available_images) - self._used_images - self._reserved_colors
            if available:
                color = list(available)[randint(0, len(available) - 1)]
            else:
                color = (randint(1, 255), randint(1, 255), randint(1, 255),)
                while color in self._used_images:
                    color = (randint(1, 255), randint(1, 255), randint(1, 255),)

        if color not in self._available_images:
            im = Image.new("RGBA", (self.DIAMETER + 1, self.DIAMETER + 1,), (0, 0, 0, 0))
            file_name = os.path.join(
                self._folder,
                "{r}_{g}_{b}.png".format(
                    r=color[0],
                    g=color[1],
                    b=color[2],
                )
            )

            draw = ImageDraw.Draw(im)
            draw.ellipse(
                [(0, 0,), (self.DIAMETER, self.DIAMETER,)],
                fill=color,
                outline=color
            )
            del draw

            with open(file_name, "wb") as fp:
                im.save(fp, format="png")
                fp.flush()

            self._available_images[color] = file_name

        self._used_images.add(color)
        return os.path.join(self._folder, self._available_images[color])

    def add_reserved(self, color):
        self._reserved_colors.add(color)

    def get_created_images(self):
        result = {}

        file_list = os.listdir(self._folder)
        for file_name in file_list:
            name = os.path.splitext(file_name)[0]

            red, green, blue = [int(i) for i in name.split("_", 2)]

            color = (red, green, blue,)
            result[color] = file_name
        return result
