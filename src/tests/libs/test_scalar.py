#!/usr/bin/env python3
import math

from unittest import TestCase

from libs.scalar import Point, Path, Vector

from libs.scalar import PathInitializationException, PointAddException, PathAddException, VectorAddException


class TestPoint(TestCase):
    def test_init_positive(self):
        point = Point(2, 3)
        self.assertEqual(point.x, 2)
        self.assertEqual(point.y, 3)
        self.assertEqual(point.coordinates, (2, 3,))

    def test_negative_no_point_length_direction(self):
        with self.assertRaises(PathInitializationException):
            Path(Point(-2, -3))

    def test_negative_no_point_length(self):
        with self.assertRaises(PathInitializationException):
            Path(Point(-2, -3), direction=1)

    def test_negative_no_point_direction(self):
        with self.assertRaises(PathInitializationException):
            Path(Point(-2, -3), length=1)

    def test_add_vector(self):
        path = Point(0, 1) + Vector(length=3, direction=math.radians(0))
        self.assertEqual(path.start_point.x, 0)
        self.assertEqual(path.start_point.y, 1)
        self.assertEqual(path.length, 3)
        self.assertEqual(path.direction, 0)

    def test_add_point(self):
        path = Point(0, 1) + Point(3, 5)
        self.assertEqual(path.start_point.x, 0)
        self.assertEqual(path.start_point.y, 1)
        self.assertEqual(path.final_point.x, 3)
        self.assertEqual(path.final_point.y, 5)

    def test_add_item(self):
        with self.assertRaises(PointAddException):
            Path(Point(0, 1) + 6)

    def test_str(self):
        point = Point(2, 3)
        self.assertEqual(str(point), "<Point (2; 3)>")

    def test_repr(self):
        point = Point(2, 3)
        self.assertEqual(repr(point), "<Point (2; 3)>")


class TestPath(TestCase):
    def test_point_init_positive(self):
        path = Path(Point(2, 3), Point(5, 3))
        self.assertEqual(path.start_point.x, 2)
        self.assertEqual(path.start_point.y, 3)
        self.assertEqual(path.final_point.x, 5)
        self.assertEqual(path.final_point.y, 3)
        self.assertEqual(path.length, 3)
        self.assertEqual(path.direction, 0)

    def test_len_direction_init_positive(self):
        path = Path(Point(2, 3), length=5, direction=math.asin(4 / 5))
        self.assertEqual(path.start_point.x, 2)
        self.assertEqual(path.start_point.y, 3)
        self.assertEqual(path.length, 5)
        self.assertEqual(path.direction, math.asin(4 / 5))
        self.assertEqual(math.ceil(path.final_point.x), 5)
        self.assertEqual(path.final_point.y, 7)

    def test_negative_no_point_length_direction(self):
        with self.assertRaises(PathInitializationException):
            Path(Point(-2, -3))

    def test_negative_no_point_length(self):
        with self.assertRaises(PathInitializationException):
            Path(Point(-2, -3), direction=1)

    def test_negative_no_point_direction(self):
        with self.assertRaises(PathInitializationException):
            Path(Point(-2, -3), length=1)

    def test_add_point(self):
        result_path = Path(Point(0, 1), Point(2, 3)) + Point(4, 5)
        self.assertEqual(result_path.start_point.x, 0)
        self.assertEqual(result_path.start_point.y, 1)
        self.assertEqual(result_path.final_point.x, 4)
        self.assertEqual(result_path.final_point.y, 5)

    def test_add_vector(self):
        path = Path(Point(0, 0), Point(2, 3)) + Vector(length=5, direction=math.radians(0))
        self.assertEqual(path.start_point.x, 0)
        self.assertEqual(path.start_point.y, 0)
        self.assertEqual(path.final_point.x, 7)
        self.assertEqual(path.final_point.y, 3)
        self.assertEqual(path.length, 7.615773105863909)
        self.assertEqual(path.direction, 0.4048917862850834)

    def test_add_item(self):
        with self.assertRaises(PathAddException):
            Path(start_position=Point(0, 1), length=5, direction=math.radians(0)) + 6

    def test_str(self):
        path = Path(Point(2, 3), Point(5, 3))
        self.assertEqual(str(path), "<Path (2; 3)->(5; 3), length 3.0, direction 0>")

    def test_repr(self):
        path = Path(Point(2, 3), Point(5, 3))
        self.assertEqual(repr(path), "<Path (2; 3)->(5; 3), length 3.0, direction 0>")

    def test_check_variable_direction_0(self):
        path = Path(start_position=Point(1, 2), length=10, direction=math.radians(0))
        self.assertEqual(path.final_point.x, 11)
        self.assertEqual(path.final_point.y, 2)

    def test_check_variable_direction_45(self):
        path = Path(start_position=Point(0, 0), length=10, direction=math.radians(45))
        self.assertEqual(path.final_point.x, 7.071067811865475)
        self.assertEqual(path.final_point.y, 7.071067811865475)

    def test_check_variable_direction_90(self):
        path = Path(start_position=Point(1, 2), length=10, direction=math.radians(90))
        self.assertEqual(path.final_point.x, 1)
        self.assertEqual(path.final_point.y, 12)

    def test_check_variable_direction_135(self):
        path = Path(start_position=Point(1, 2), length=10, direction=math.radians(125))
        self.assertEqual(path.final_point.x, -4.735764363510462)
        self.assertEqual(path.final_point.y, 10.191520442889917)

    def test_check_variable_direction_180(self):
        path = Path(start_position=Point(1, 2), length=10, direction=math.radians(180))
        self.assertEqual(path.final_point.x, -9)
        self.assertEqual(path.final_point.y, 2)

    def test_check_variable_direction_225(self):
        path = Path(start_position=Point(1, 2), length=10, direction=math.radians(225))
        self.assertEqual(path.final_point.x, -6.071067811865475)
        self.assertEqual(path.final_point.y, -5.071067811865475)

    def test_check_variable_direction_270(self):
        path = Path(start_position=Point(1, 2), length=10, direction=math.radians(270))
        self.assertEqual(path.final_point.x, 1)
        self.assertEqual(path.final_point.y, -8)

    def test_check_variable_direction_315(self):
        path = Path(start_position=Point(1, 2), length=10, direction=math.radians(315))
        self.assertEqual(path.final_point.x, 8.071067811865476)
        self.assertEqual(path.final_point.y, -5.071067811865475)

    def test_check_variable_direction_360(self):
        path = Path(start_position=Point(1, 2), length=10, direction=math.radians(360))
        self.assertEqual(path.final_point.x, 11)
        self.assertEqual(path.final_point.y, 2)

    def test_check_direction_0(self):
        path = Path(Point(1, 1), Point(1, 1))
        self.assertEqual(path.direction, 0)

    def test_check_direction_45(self):
        path = Path(Point(1, 1), Point(5, 5))
        self.assertEqual(path.direction, 0.7853981633974482)

    def test_check_direction_90(self):
        path = Path(Point(1, 1), Point(1, 5))
        self.assertEqual(path.direction, 1.5707963267948966)

    def test_check_direction_135(self):
        path = Path(Point(1, 1), Point(-1, 5))
        self.assertEqual(path.direction, 2.0344439357957027)

    def test_check_direction_180(self):
        path = Path(Point(1, 1), Point(-5, 1))
        self.assertEqual(path.direction, 3.141592653589793)

    def test_check_direction_225(self):
        path = Path(Point(1, 1), Point(-1, -5))
        self.assertEqual(path.direction, 4.3906384259880475)

    def test_check_direction_270(self):
        path = Path(Point(1, 1), Point(1, -5))
        self.assertEqual(path.direction, 4.71238898038469)

    def test_check_direction_315(self):
        path = Path(Point(1, 1), Point(5, -5))
        self.assertEqual(path.direction, 5.300391583932257)


class TestVector(TestCase):
    def test_init_positive(self):
        vector = Vector(length=5, direction=math.radians(45))
        self.assertEqual(vector.length, 5)

    def test_init_positive_direction(self):
        vector = Vector(length=5, direction=math.radians(45))
        self.assertEqual(vector.direction, 0.7853981633974483)

    def test_add_vector(self):
        vector = Vector(length=5, direction=math.radians(90)) + Vector(length=10, direction=math.radians(0))
        self.assertEqual(vector.length, 11.180339887498949)
        self.assertEqual(vector.direction, 0.4636476090008061)

    def test_add_path(self):
        vector = Vector(length=10, direction=math.radians(0)) + Path(Point(0, 1), Point(3, 5))
        self.assertEqual(vector.length, 13.601470508735444)
        self.assertEqual(vector.direction, 0.2984989315861793)

    def test_add_point(self):
        vector = Vector(length=5, direction=math.radians(90)) + Point(0, 1)
        self.assertEqual(vector.length, 5)
        self.assertEqual(vector.direction, 1.5707963267948966)

    def test_add_item(self):
        with self.assertRaises(VectorAddException):
            Vector(Vector(length=5, direction=math.radians(0)) + 6)
