#!/usr/bin/env python3

import os
import math
from libs.scene import Scene, SceneActor, Amoeba, ImageProvider

from random import randint, random

if __name__ == '__main__':
    BASE_DIR = os.path.dirname(os.path.abspath(__file__))
    image_provider = ImageProvider(image_folder=os.path.join(BASE_DIR, "amoebas"))

    scene = Scene(name="Scene", width=1000, height=1000, debug=False)

    for i in range(randint(30, 100)):
        a0 = Amoeba(weight_kg=randint(1, 500) / 1000 / 1000 / 1000)
        a0.force = a0.mass * 1000
        #         a0.force = 0.00005
        a0.force_angle = math.radians(360 * random())
        scene.add_scene_actor(a0)

    # a0 = Amoeba(weight_kg=100 / 1000 / 1000 / 1000)
    # a0.position = (500, 500)
    # a0.force = a0.mass * 1000
    # a0.force = 0.00001
    # a0._speed = 20
    # a0.force_angle = math.radians(360 * random())
    # a0.force_angle = 0
    # scene.add_scene_actor(a0)

    scene.run()
